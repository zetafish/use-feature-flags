set -eu

cat <<EOF > /kaniko/.docker/config.json
{
  "auths":{
    "$CI_REGISTRY":{
      "username":"${CI_REGISTRY_USER}",
      "password":"${CI_REGISTRY_PASSWORD}"
    }
  }
}
EOF

/kaniko/executor \
    --cache \
    --context $CI_PROJECT_DIR \
    --dockerfile $CI_PROJECT_DIR/Dockerfile \
    --destination $CI_REGISTRY_IMAGE:$VERSION
