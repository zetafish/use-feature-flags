FROM clojure:openjdk-11-tools-deps-1.10.1.469
WORKDIR /app
COPY deps.edn .
RUN clojure -Stree
COPY VERSION .
COPY src/ src
CMD ["clojure", "-A:app"]
